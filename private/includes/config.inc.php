<?php

#define('MOBILE_SITE_URL', 'm.site_name.com');
error_reporting(E_ALL & ~E_STRICT);
if(!empty($_SERVER['ELYK_ENV']))
    define('ELYK_ENV', $_SERVER['ELYK_ENV']);
else
    define('ELYK_ENV', 'production');

if(!isset($config_exclusions))
	$config_exclusions=array('wordpress');

if(!in_array('session', $config_exclusions))
{
    session_start();
}
ini_set('default_charset', 'UTF-8');
date_default_timezone_set('America/New_York');

$config_suffixes = array('-env', '');
foreach($config_suffixes as $suffix)
{
    if(empty($config_xml_file))
    {
        $config_xml=$_SERVER['DOCUMENT_ROOT'].'/../private/config'.$suffix.(!empty($suffix) && ELYK_ENV!=''?'-'.ELYK_ENV :'').'.xml';
    }
    else
        $config_xml=$_SERVER['DOCUMENT_ROOT'].'/../private/'.$config_xml_file;

    if(file_exists($config_xml) && is_file($config_xml))
    {
        $config_xml = simplexml_load_string(file_get_contents($config_xml));
    }
    else
        continue;
    foreach($config_xml->constant as $constant)
    {
        if (!defined((string)$constant['name']))
            define((string)$constant['name'], (string)$constant);
    }

    if (defined('MODULE_ROOT') && defined('DOCUMENT_ROOT') && !defined('MODULE_ROOT_TRANSLATED'))
        define('MODULE_ROOT_TRANSLATED', '/'.str_replace(DOCUMENT_ROOT, '', MODULE_ROOT));

    foreach($config_xml->class as $class)
        require_once(CLASS_ROOT.$class['file']);
}

define('TABMIN', (preg_match('/^\/tabmin\//', $_SERVER['PHP_SELF'])!=0));

define('DEFAULT_TEMPLATE', $_SERVER['DOCUMENT_ROOT'].'/template.php');


if(!in_array('wordpress', $config_exclusions))
{
	if(!defined('WP_USE_THEMES') && defined('WP_ROOT') && !TABMIN)
	{
		define('WP_USE_THEMES', false);
		require WP_ROOT.'wp-load.php';
	}
}

require_once GLOBAL_ROOT.'classes/ErrorSet.inc.php';
require_once GLOBAL_ROOT.'classes/Tabmin.inc.php';
require_once GLOBAL_ROOT.'classes/AlertSet.inc.php';
require_once GLOBAL_ROOT.'classes/XSRF.inc.php';
require_once GLOBAL_ROOT.'classes/TemplateSet.inc.php';
require_once GLOBAL_ROOT.'util.inc.php';
require_once GLOBAL_ROOT.'pdo.inc.php';

try
{
	Tabmin::$db = new PDO('mysql:host='.TABMIN_DB_HOST.';dbname='.TABMIN_DB_NAME.';charset=utf8', TABMIN_DB_USERNAME, TABMIN_DB_PASSWORD);
	Tabmin::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	Tabmin::$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
}
catch (Exception $e)
{
    print_r($e);
	die('can not create db connection');
	//error handler here
}

if(DEBUG)
{
    ErrorSet::$display=true;
    ErrorSet::$email = null;
}
else
	ErrorSet::$email=ERROR_REPORTING_EMAIL;

if(get_magic_quotes_gpc())
{
	$_GET=stripslashes_deep($_GET);
	$_POST=stripslashes_deep($_POST);
	$_COOKIE=stripslashes_deep($_COOKIE);
}

if(@$_POST['verb']=='login')
{
	if(XSRF::valid())
	{
        if(empty($_POST['password']))
        {
            AlertSet::addValidation('Password is required');
        }
        else
        {
            $user = Users::login($_POST['email'], $_POST['password']);
            if($user)
            {
                $_SESSION['tabmin_users_id'] = $user->getId();

                if($user->getRole()->getDeveloper())
                    $_SESSION['tabmin_developer']=1;
            }
            else
            {
                if(AlertSet::$success)
                {
                    AlertSet::addError("Login failed.");
                }
            }
        }
	}
	else
		AlertSet::addError(XSRF::GENERIC_ERROR);
		
}
if(@$_POST['verb']=='switch-user')
{
	if(@$_SESSION['tabmin_developer'])
	{		
		$_SESSION['tabmin_users_id']=intval($_POST['users_id']);
	}
}

if(isset($_GET['logout']))
{
    AuditTrail::log(new User(array('id'=>$_SESSION['tabmin_users_id'])), AuditTypes::LOGOUT, 'User logged out');
	unset($_SESSION['tabmin_users_id']);	unset($_SESSION['tabmin_developer']);
	AlertSet::addSuccess('You have been logged out');
	header('Location: /');
	exit();
}

if(isset($_GET['forgotpw']))
{	
	header('Location: ../forgot-pw.php');
	exit();
}

global $currentUser, $modules;
if(!empty($_SESSION['tabmin_users_id']))
{
	if($currentUser=Users::loadById($_SESSION['tabmin_users_id']))
	{
		Permissions::PermissionsByRole($currentUser->getRole()->getId(), @$_SESSION['tabmin_developer']);
	}
}

if(!empty($currentUser))
{
	if($currentUser->getRole()->isDeveloper() || @$_SESSION['tabmin_developer'])
	{
		ErrorSet::$display=true;
		ErrorSet::$email=null;
	}


    $modules=Tabmin::load($config_xml->modules, $currentUser);
    $module=false;
    $tab=false;

    $module_name=preg_replace('/^(.*?\/)+/', '', dirname($_SERVER['SCRIPT_NAME']));
    foreach($modules as $a_module)
    {
        if($a_module->name==$module_name)
        {
            $module=$a_module;
            break;
        }
    }

    if($module)
    {
        $tab_name=basename($_SERVER['SCRIPT_NAME']);
        foreach($module->tabs as $a_tab)
        {
            if($a_tab->file==$tab_name)
            {
                $tab=$a_tab;
                break;
            }
        }
    }
	
	if(defined('MOBILE_SITE_URL'))
	{
		require_once GLOBAL_ROOT.'mobile_device_detect.php';
		if(isset($_GET['full']))
			$_SESSION['full_site']=true;
		if(empty($_SESSION['full_site']))
			mobile_device_detect(true, true, true, true, true, true, MOBILE_SITE_URL);
	}
}
?>
