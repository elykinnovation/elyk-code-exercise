<?php
class Printer
{
    public static function printString($string)
    {
        echo (htmlentitiesUTF8($string));
    }

    public static function printDate($date, $format='m/d/Y')
    {
        echo (date($format, $date));
    }

    public static function printDec($num, $dec='2')
    {
        echo (number_format(doubleval($num), $dec));
    }

    public static function printInt($num, $dec='2')
    {
        echo (number_format(intval($num), $dec));
    }

}

class P
{
    public static function out($data, $isDate = false, $dateFormat = 'm/d/Y', $dblDec = 2)
    {
        echo P::sanitize($data, $isDate, $dateFormat, $dblDec);
    }

    public static function sanitize($data, $isDate = false, $dateFormat = 'm/d/Y', $dblDec = 2)
    {
        if ($isDate)
            return date($dateFormat, $data);
        else
        {
            if (is_numeric($data) && (intval($data) == floatval($data)))
                return (intval($data));
            else if (is_numeric($data))
                return number_format(floatval($data), $dblDec);
            else if(is_string($data))
                return (htmlentitiesUTF8($data));
        }
    }


    public static function truncate($data, $length=40)
    {
        $data = P::sanitize($data);

        if( strlen($data) > $length )
        {
            echo substr($data, 0, $length) . '...';
        }
        else
        {
            echo $data;
        }
    }


    /**
     * displays a strip of pagination links
     * @param $page current page
     * @param $total_pages total number of possible pages
     */
    public static function pagination($page, $total_pages, $get_vars)
    {
        // we display pagination if there are more than one page
        if ($total_pages > 1)
        {
            $get = array();
            foreach ($get_vars as $key=>$g)
            {
                if ($key != 'page_number' && $g != '')
                {
                    if (is_array($g))
                    {
                        foreach ($g as $g_array_item)
                        {
                            $get[] = $key.'[]='.$g_array_item;
                        }
                    }
                    else
                    {
                        $get[] = $key.'='.$g;
                    }

                }
            }
            $get = '?'.implode('&',$get);
            if (strlen($get) > 1)
            {
                $get .= '&';
            }


            $maxPages = 7; // display max of 7 numbered pages ex: 1...4 5 6 7 8 9 10..19 ( we have 7 pages between two "...")
            $pageOffset = $maxPages % 2 == 1 ? ($maxPages - 1) / 2 : $maxPages / 2; // compute pageOffset which is the number of pages from the center

            $uri = preg_replace("/([\?].*)/", "", $_SERVER['REQUEST_URI']);

            // if page is greater than, display "previous" link
            if($page > 1)
            {
                ?>
                <a href="<?php P::out($uri.$get)?>page_number=<?php P::out($page-1)?>">
                    &laquo; Prev
                </a>
            <?php
            }

            // always show page 1
            ?>
            <a href="<?php P::out($uri.$get)?>page_number=1" <?php echo ( $page == 1  ? ' class="active"' : '') ?>>1</a>
            <?php

            // decide the start page# and end page# for page listing in the middle
            $page_start = 2;
            if ($total_pages > $maxPages)
            {
                $page_start = $page - $pageOffset > 1 ? $page - $pageOffset : '2';
            }
            $page_end = $page_start + $maxPages - 1;

            if ($page_end >= $total_pages)
            {
                $page_end = $total_pages - 1;
            }

            //if we have a gap between page 1 and middle section start, display "..."
            if ($page_start > 2)
            {
                ?>
                ...
            <?php
            }

            //show the middle section of pagination strip
            for($i = $page_start; $i <= $page_end; $i++)
            {
                ?>
                <a href="<?php P::out($uri.$get)?>page_number=<?php P::out($i) ?>" <?php echo ( $page == $i  ? ' class="active"' : '') ?>><?php P::out($i)?></a>
            <?php
            }

            //if we have a gap between middle section end page # and last page, display "..."
            if ($page_end + 1 < $total_pages)
            {
                ?>
                ...
            <?php
            }
            // always show last page
            ?>
            <a href="<?php P::out($uri.$get)?>page_number=<?php P::out(intval($total_pages)) ?>" <?php echo ( $page == $total_pages  ? ' class="active"' : '') ?>><?php P::out(intval($total_pages))?></a>
            <?php

            //display "Next" link if we can advance to the next page
            if($page < $total_pages)
            {
                ?>
                <a href="<?php P::out($uri.$get)?>page_number=<?php P::out($page+1)?>">
                    Next &raquo;
                </a>
            <?php
            }
        }
    }


    /**
     * displays a strip of pagination links
     * @param $page current page
     * @param $total_pages total number of possible pages
     */
}

?>