<?php
class UserController extends Controller
{
	
	protected function loadTobject($properties)
	{
		$this->tobject = new User($properties);
	}
	
	public function add()
	{
		if ($this->properties['password'] != $this->properties['password2'])
        {
            AlertSet::addValidation('Password must match');
        }

		parent::add();
	}
	
	public function update()
	{		
		if (!empty($this->properties['password']) && ($this->properties['password'] != $this->properties['password2']))
        {
            AlertSet::addValidation('Password must match');
        }
		
		parent::update();
	}
	
	public function forgotPassword()
	{
		if(!empty($this->properties['email']))
		{
			if(Users::sendforgotPasswordLink($this->properties['email']))
			{
				$this->$json['success'] = true;
				AlertSet::addSuccess('You have been sent an email from '.SITE_NAME.' with further instructions. Please follow the link in the email to reset your password. If the email does not appear in your inbox, please check any spam folders.');
			}
			else
				AlertSet::addError('We were not able to send the email to the specified address. Please make sure that the email address you are using is the same address you have provided and try again.');
		}
		else
			AlertSet::addValidation('You must enter your email address.');
	}
	
	public function forgotPasswordAdmin()
	{
		$user = Users::loadById(intval($this->properties['id']));
		if(!empty($user->email))
		{
			if(Users::sendforgotPasswordLink($user->email))
			{
				$this->$json['success'] = true;
				AlertSet::addSuccess('You have sent a forgot password email to this user.');
			}
			else
				AlertSet::addError('We were not able to send the email to the specified address. Please make sure that the email address you are using is valid and try again.');
		}
		else
			AlertSet::addValidation('User must have an email address to send forgot password link.');
		
	}
	
	public function resetPassword()
	{
		if($this->properties['password'] == $this->properties['password2'] && !empty($this->properties['password']))
		{
			if(Users::setPassword($this->properties['key'], $this->properties['password']))
			{
				$this->$json['success'] = true;
				AlertSet::addSuccess('Your password has been changed. You may now log in.');
			}
			else
				AlertSet::addError('There was an error changing your password.');
		}
		else
			AlertSet::addError('Passwords do not match.');
	}
	
}
?>