<?php

class Breadcrumbs
{
    protected static $breadcrumbs = array();
    protected static $html_template_with_url = '<li><a href="[[URL]]">[[NAME]]</a></li>';
    protected static $html_template_without_url = '<li>[[NAME]]</li>';
    protected static $separator = '';

    public static function push($name, $url=null)
    {
        self::$breadcrumbs[] = array
        (
            'name'=>$name,
            'url'=>$url
        );
    }

    public static function out()
    {
        foreach(self::$breadcrumbs as $i=>$breadcrumb)
        {
            if(!empty($breadcrumb['url']))
            {
                $out = str_replace('[[URL]]', $breadcrumb['url'], self::$html_template_with_url);
                $out = str_replace('[[NAME]]', $breadcrumb['name'], $out);
            }
            else
            {
                $out = str_replace('[[NAME]]', $breadcrumb['name'], self::$html_template_without_url);
            }
            if($i!=0)
            {
                echo self::$separator;
            }
            echo $out;
        }
    }
}
?>