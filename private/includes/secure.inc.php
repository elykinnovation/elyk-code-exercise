<?php

if(empty($currentUser))
{
	AlertSet::addError('You have been logged out. Please log in again.');
	AlertSet::save();
	header('Location: /');
	exit();
}
else if(!empty($tab) && !$currentUser->tabPermission($module, $tab) || $tab===false || $module===false)
{
    TemplateSet::begin('body');
	echo 'You do not have permission to view this page. (Module: '.($module?:'FALSE').', Tab: '.($tab?:'FALSE').')';
    TemplateSet::end();
    TemplateSet::display(DOCUMENT_ROOT.'/template.php');
	exit();
}
?>