<?php


/**
 * Class ErrorSet
 */
class ErrorSet
{
    const VERSION = 2.0;

    const EMAIL_FORMAT_JSON = 1;
    const EMAIL_FORMAT_NORMAL = 2;

    private static $constants = array
    (
        1 => 'Fatal Error -- E_ERROR',
        2 => 'E_WARNING',
        4 => 'E_PARSE',
        8 => 'E_NOTICE',
        16 => 'Fatal Error -- E_CORE_ERROR',
        32 => 'E_CORE_WARNING',
        64 => 'Fatal Error -- E_COMPILE_ERROR',
        128 => 'E_COMPILE_WARNING',
        256 => 'E_USER_ERROR',
        512 => 'E_USER_WARNING',
        1024 => 'E_USER_NOTICE',
        2048 => 'E_STRICT',
        4096 => 'E_RECOVERABLE_ERROR',
        8192 => 'E_DEPRECATED',
        16384 => 'E_USER_DEPRECATED',
        30719 => 'E_ALL'
    );

    private static $email_encryption_key =
        '[PASTE_PRIVATE_KEY_HERE]';

    private static $errors = array();
    private static $email_success = null;

    public static $display = false;
    public static $json = array();
    public static $ajax = false;
    public static $email = NULL;
    public static $email_subject = NULL;
    public static $encrypt_email = false;
    public static $backtrace = array();
    public static $start_timestamp = NULL;
    public static $old_handler;
    public static $email_format = self::EMAIL_FORMAT_JSON;

    /**
     * Disabled the error handle by restoring the previous one
     */
    public static function disable()
    {
        restore_error_handler();
    }

    /**
     * @param $errno int Error Number
     * @param $errstr string Error Message
     * @param $errfile string File in which the error occurred
     * @param $errline int Line on which the error occurred
     *
     * @return bool Always returns true
     */
    public static function error_handler($errno, $errstr, $errfile, $errline)
    {

        //Console::add('error_reporting() & $errno: '.(error_reporting() & $errno), 'Error No: '.$errno, 'Error Reporting: '.error_reporting());
        if (error_reporting() & $errno) {
            if ($errno != E_STRICT) {
                $i = count(self::$errors);
                self::$errors[$i]['type'] = $errno;
                self::$errors[$i]['message'] = $errstr;
                self::$errors[$i]['file'] = $errfile;
                self::$errors[$i]['line'] = $errline;
                self::$backtrace[$i] = debug_backtrace();
            }
        }
        return true;
    }

    /**
     * Adds errors, console and SQL log to AlertSet
     */
    public static function addErrorsToAlertSet()
    {
        for ($i = 0; $i < count(self::$errors); $i++) {
            switch (self::$errors[$i]['type']) {
                case E_USER_ERROR:
                    $type = 'ERROR: ';
                    break;
                case E_WARNING:
                case E_USER_WARNING:
                    $type = 'WARNING: ';
                    break;
                case E_USER_NOTICE:
                    $type = 'NOTICE: ';
                    break;
                default:
                    if (isset(self::$constants[self::$errors[$i]['type']]))
                        $type = self::$constants[self::$errors[$i]['type']] . ':';
                    break;
            }

            AlertSet::addError($type . self::$errors[$i]['message'] . ' on line ' . self::$errors[$i]['line'] . ' in file ' . self::$errors[$i]['file']);
        }
    }

    /**
     * Sets AJAX mode
     *
     * @param $b bool
     */
    public static function setAJAX($b)
    {
        if ($b) {
            ob_start();
            self::$ajax = true;
        } else {
            if (self::$ajax)
                ob_end_clean();
            self::$ajax = false;
        }
    }

    /**
     * Creates JSON containing information about the current request.
     *
     * @return array
     */
    public static function getHeaderJSON()
    {

        $json = array();
        $json['url'] = empty($_SERVER['REQUEST_URI']) ? $_SERVER['SCRIPT_FILENAME'] : $_SERVER['REQUEST_URI'];
        $json['method'] = $_SERVER['REQUEST_METHOD'];
        $json['referrer'] = empty($_SERVER['HTTP_REFERER']) ? '' : $_SERVER['HTTP_REFERER'];
        $json['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
        $json['remote_address'] = $_SERVER['REMOTE_ADDR'];
        $json['request_time'] = (microtime(true) - self::$start_timestamp);

        return $json;
    }

    /**
     * Formats the header JSON from {@link getHeaderJSON} in HTML
     *
     * @return string
     */
    public static function getHeaderHTML()
    {
        $json = self::getHeaderJSON();
        $table = '';
        $table .= '<strong>Page requested</strong>: ' . $json['url'] . '<br />';
        $table .= '<strong>Request Method</strong>: ' . $json['method'] . '<br />';
        $table .= '<strong>Referrer</strong>: ' . $json['referrer'] . '<br />';
        $table .= '<strong>User Agent</strong>: ' . $json['user_agent'] . '<br />';
        $table .= '<strong>Remote Address</strong>: ' . $json['remote_address'] . '<br />';
        $table .= '<strong>Request Time</strong>: ' . $json['request_time'] . ' seconds<br />';
        if(self::$email_success!==null)
        {
            $table .= 'Email sent to <strong>'.self::$email.'</strong> and was a <strong>'.(self::$email_success ? 'success' : 'failure').'</strong>';
        }
        return $table;
    }

    /**
     * Returns an array of errors that occurred on the page with
     * type, message, line, file, and backtrace
     *
     * @return array
     */
    public static function getErrorJSON()
    {
        $array = array();
        if (count(self::$errors) > 0)
        {
            for ($i = 0; $i < count(self::$errors); $i++)
            {
                $error = array
                (
                    'type'=>self::$constants[self::$errors[$i]['type']],
                    'message'=>self::$errors[$i]['message'],
                    'line'=>self::$errors[$i]['line'],
                    'file'=>self::$errors[$i]['file'],
                    'backtrace'=>self::$backtrace[$i]
                );
                $array[] = $error;

            }
        }

        return $array;
    }

    /**
     * Returns HTML string of errors
     * @return string
     */
    public static function getErrorHTML()
    {
        $table = '
        <style>
            .ErrorSet_errors{
                border-bottom: 1px solid;
                width: 100%;
                font-family: "Courier New";
                font-size: 11px;
            }

            .ErrorSet_errors .backtrace td,
            .ErrorSet_errors .backtrace th {
                white-space: nowrap;
                vertical-align: top;
                font-family: "Courier New";
                font-size: 11px;
            }

            .ErrorSet_console,
            .ErrorSet_query_log,
            .ErrorSet_error_container {
                border: 1px solid;
                min-height: 14px;
                background-color: #D1D3FF;
                padding: 5px 0px 5px 10px;
                margin-top: 10px;
                color: black;
                font-family: "Courier New";
                font-size: 11px;
            }

            .ErrorSet_query_log {
                background-color: #fd9;
                color: #b64;
            }

            .ErrorSet_error_container {
                background-color: #fcc;
                color: #D8000C;
                padding-left: 0px;
            }

            .ErrorSet_console pre,
            .ErrorSet_query_log pre {
                padding: 0px;
                margin: 0px;
            }

            .ErrorSet_console p,
            .ErrorSet_query_log p {
                padding: 0px 0px 0px 0px;
                margin: 0px 0px 10px 0px;
                font-weight: bold;
                font-size: 11px;
            }

            .ErrorSet_query_log hr {
                height: 0;
                border-top: 1px solid #b64;
            }

            .ErrorSet_headers {
                margin: 5px 0;
                padding: 5px;
                background-color: #FFCCCC;
                display: block;
                border: 1px solid;
            }
        </style>';

        if (count(self::$errors) > 0) {
            for ($i = 0; $i < count(self::$errors); $i++) {
                $table .= '<table class="ErrorSet_errors">';
                $table .= '
                    <tr>
                        <td>';
                switch (self::$errors[$i]['type']) {
                    case E_USER_ERROR:
                        $table .= '<b style="color:#900">ERROR: </b>';
                        break;

                    case E_WARNING:
                    case E_USER_WARNING:
                        $table .= '<b style="color:#990">WARNING: </b>';
                        break;
                    case E_USER_NOTICE:
                        $table .= '<b style="color: #009">NOTICE: </b>';
                        break;

                    default:
                        if (isset(self::$constants[self::$errors[$i]['type']]))
                            $table .= '<b>' . self::$constants[self::$errors[$i]['type']] . ': </b>';
                        break;
                }
                $table .=
                    self::$errors[$i]['message'] . ' on line <strong>' . self::$errors[$i]['line'] . '</strong> in file <strong>' . self::$errors[$i]['file'] . '</strong></td>
                </tr>';
                $table .= '<tr><td colspan="6">' . self::getBacktraceHTML(self::$backtrace[$i]) . '</td></tr>';
                $table .= '</table>';
            }
        } else
            $table .= 'You\'ve finally done it right!!';
        return $table;
    }

    /**
     * Returns true if there are errors on the page. False otherwise.
     * @return bool
     */
    public static function hasErrors()
    {
        return count(self::$errors) > 0;
    }

    /**
     * This is the PHP shutdown function and handles printing everything to the page and
     * sending emails if necessary
     */
    public static function shutdown_function()
    {
        if ($error = error_get_last())
        {
            switch ($error['type'])
            {
                case E_ERROR:
                case E_CORE_ERROR:
                case E_COMPILE_ERROR:
                case E_USER_ERROR:
                    ob_end_clean();
                    $i = count(self::$errors);
                    self::$errors[$i]['type'] = $error['type'];
                    self::$errors[$i]['message'] = $error['message'];
                    self::$errors[$i]['file'] = $error['file'];
                    self::$errors[$i]['line'] = $error['line'];
                    self::$backtrace[$i] = debug_backtrace();
                    break;
            }
        }

        $error_html = self::getErrorHTML();

        if (self::$email !== NULL && (count(self::$errors) > 0 || !Console::isEmpty()))
        {
            self::sendEmail();
        }

        if (self::$display)
        {
            if (self::$ajax)
            {
                self::$json = json_decode(ob_get_clean(), true);
                self::addErrorsToAlertSet();
                Console::addToAlertSet();
                QueryLog::addToAlertSet();
                self::$json['alerts'] = AlertSet::$alerts;
                echo json_encode(self::$json);
            }
            else
            {
                echo '
                <div class="ErrorSet">
                    <div>
                        <div>
                            <span class="ErrorSet_headers">' . self::getHeaderHTML() . '</span>
                            <div class="ErrorSet_inner_tab' . (self::hasErrors() ? ' ErrorSet_inner_tab_selected' : '') . '">Errors</div>
                            <div class="ErrorSet_inner_tab' . (!self::hasErrors() ? ' ErrorSet_inner_tab_selected' : '') . '">Console</div>
                            <div class="ErrorSet_inner_tab">Query Log</div>
                            <div class="ErrorSet_inner_content">
                            <div class="ErrorSet_error_container">' . $error_html . '</div>
                                <div class="ErrorSet_console">' . Console::toString() . '</div>
                                <div class="ErrorSet_query_log">' . QueryLog::toString() . '</div>
                            </div>
                        </div>
                    </div>
                </div><div class="ErrorSet_tab" style="display:none;" title="' . htmlentities($_SERVER['SCRIPT_NAME']) . '"><b>[' . (count(self::$errors)) . ', <span style="color: #0000AA">' . Console::size() . '</span>, <span style="color: #b64">' . QueryLog::size() . '</span>]</b> <span class="ErrorSet_url">' . htmlentities($_SERVER['SCRIPT_NAME']) . '</span></div>

                <style type="text/css">
                    .ErrorSet {
                        min-height: 15px;
                        margin: 0 0 0 0;
                        padding: 0;
                        bottom: 25px;
                        left: 0px;
                        width: 97%;
                        display: none;
                        position: fixed;
                        z-index: 1000;
                    }

                    .ErrorSet_selected {
                        display: block;
                    }

                    .ErrorSet > div {
                        margin: 0 auto;
                        padding: 10px;
                        max-height: 200px;
                        overflow: auto;
                        width: 100%;
                        color: #D8000C;
                        border: 1px solid;
                        background-color: #FFBABA;
                    }

                    .ErrorSet_tab {
                        width: 52px;
                        text-align: center;
                        height: 13px;
                        font-size: 12px;
                        font-family: "Tahoma", "Verdana", "Sans-Serif";
                        margin: 0 5px 0 0;
                        padding: 6px;
                        overflow: hidden;
                        position: fixed;
                        bottom: 0px;
                        background-color: #FFBABA;
                        color: #D8000C;
                        border: 1px solid;
                        white-space: nowrap;
                        filter: alpha(opacity=65);	/* For IE */
                        opacity: .65;				/* For everyone else */
                        cursor: pointer;
                        z-index: 100;
                    }

                    .ErrorSet_tab:hover {
                        width: auto;
                        z-index: 105;
                        filter: alpha(opacity=100);
                        opacity: 1.00;
                    }

                    .ErrorSet_tab .ErrorSet_url {
                        display: none;
                    }

                    .ErrorSet_tab:hover .ErrorSet_url {
                        display: inline;
                    }

                    .ErrorSet_tab_selected {
                        width: auto;
                        min-width: 52px;
                        border-top: none;
                        filter: alpha(opacity=100);
                        opacity: 1.00;
                        z-index: 101;
                        white-space: nowrap;
                    }

                    .ErrorSet_inner_tab {
                        float: left;
                        font-size: 12px;
                        font-family: "Tahoma", "Verdana", "Sans-Serif";
                        width: 72px;
                        height: 13px;
                        color: #D8000C;
                        border: 1px solid;
                        margin: 0 5px 0 0;
                        padding: 6px;
                        cursor: pointer;
                        border-bottom: 0 none;
                    }

                    .ErrorSet_inner_tab:hover {
                        background-color:  #FF9999;
                    }

                    .ErrorSet_inner_tab_selected {
                        background-color:#FFCCCC;
                        height: 14px;
                        position: relative;
                        top: 1px;
                        z-index: 105;
                    }

                    .ErrorSet_inner_content {
                        float: left;
                        clear: left;
                        width: 99%;
                        border: 1px solid;
                        background-color:#FFCCCC;
                        padding: 5px;
                    }

                    .ErrorSet_inner_content > div {
                        display: none;
                    }

                    .ErrorSet_inner_content div.ErrorSet_' . (self::hasErrors() ? 'error_container' : 'console') . ' {
                        display: block;
                    }

                </style>

                <script type="text/javascript">
                    if(!Function.prototype.partial)
                    {
                        Function.prototype.partial = function(/* 0..n args */)
                        {
                            var fn = this, args = Array.prototype.slice.call(arguments);
                            return function()
                            {
                                var arg = 0;
                                for(var i = 0; i < args.length && arg < arguments.length; i++)
                                {
                                    if(args[i] === undefined)
                                    args[i] = arguments[arg++];
                                }
                                return fn.apply(this, args);
                            };
                        }
                    }

                    var div, tmp_div1, tmp_div2, tab, img;

                    setInterval(function()
                    {
                        var divs, i, tab, divs2, container, j;

                        divs=document.getElementsByClassName(\'ErrorSet\');
                        for(i=0; i<divs.length; i++)
                        {
                            tab=divs[i].nextSibling;
                            tab.style.left=(i*67)+\'px\';
                            tab.style.display=\'\';

                            divs2 = divs[i].getElementsByTagName(\'div\');
                            for(j=0; j<divs2.length; j++)
                            {
                                if(divs2[j].className.indexOf(\'ErrorSet_inner_tab\')!=-1)
                                {
                                    divs2[j].onclick = function(errorset, tab)
                                    {
                                        var divs = errorset.getElementsByTagName(\'div\');
                                        var i;
                                        for(i=0; i<divs.length; i++)
                                        {
                                            var isConsole = (divs[i].className.indexOf(\'console\')!=-1 && tab.innerHTML==\'Console\');
                                            var isQueryLog = (divs[i].className.indexOf(\'query_log\')!=-1 && tab.innerHTML==\'Query Log\');
                                            var isErrorContainer = (divs[i].className.indexOf(\'error_container\')!=-1 && tab.innerHTML==\'Errors\');
                                            if(isConsole || isQueryLog || isErrorContainer)
                                            {
                                                if(tab.className.indexOf(\'selected\')==-1)
                                                {
                                                    divs[i].style.display = \'block\';
                                                    tab.className += \' ErrorSet_inner_tab_selected\';
                                                }
                                            }
                                            else if(divs[i].className.indexOf(\'console\')!=-1 || divs[i].className.indexOf(\'query_log\')!=-1 || divs[i].className.indexOf(\'error_container\')!=-1)
                                            {
                                                divs[i].style.display = \'none\';
                                            }
                                            else if(divs[i].className.indexOf(\'inner_tab\')!=-1)
                                            {
                                                divs[i].className = divs[i].className.replace(\' ErrorSet_inner_tab_selected\', \'\');
                                            }
                                        }
                                    }.partial(divs[i], divs2[j]);
                                }
                            }

                            tab.onclick=function(divs, i)
                            {
                                var j;
                                for(j=0; j<divs.length; j++)
                                {
                                    if(i==j && divs[i].className.indexOf(\'selected\')==-1)
                                    {
                                        divs[i].nextSibling.className=\'ErrorSet_tab ErrorSet_tab_selected\';
                                        divs[i].className+=\' ErrorSet_selected\';
                                        continue;
                                    }
                                    divs[j].nextSibling.className=\'ErrorSet_tab\';
                                    divs[j].className=\'ErrorSet\';
                                }
                            }.partial(divs, i);
                        }
                    }, 1000);

                    if(!document.getElementsByClassName)
                    {
                        document.getElementsByClassName = function(className)
                        {
                            var classes = className.split(/\\s+/);
                            var classesToCheck = \'\';
                            var returnElements = [];
                            var match, node, elements;

                            if (document.evaluate)
                            {
                                var xhtmlNamespace = \'http://www.w3.org/1999/xhtml\';
                                var namespaceResolver = (document.documentElement.namespaceURI === xhtmlNamespace)? xhtmlNamespace:null;

                                for(var j=0, jl=classes.length; j<jl;j+=1)
                                    classesToCheck += "[contains(concat(\' \', @class, \' \'), \' " + classes[j] + " \')]";

                                try
                                {
                                    elements = document.evaluate(".//*" + classesToCheck, document, namespaceResolver, 0, null);
                                }
                                catch(err)
                                {
                                    elements = document.evaluate(".//*" + classesToCheck, document, null, 0, null);
                                }

                                while((match = elements.iterateNext()))
                                    returnElements.push(match);
                            }
                            else
                            {
                                classesToCheck = [];
                                elements = (document.all) ? document.all : document.getElementsByTagName("*");

                                for (var k=0, kl=classes.length; k<kl; k++)
                                    classesToCheck.push(new RegExp("(^|\\\\s)" + classes[k] + "(\\\\s|$)"));

                                for (var l=0, ll=elements.length; l<ll;l+=1)
                                {
                                    node = elements[l];
                                    match = false;
                                    for (var m=0, ml=classesToCheck.length; m<ml; m+=1)
                                    {
                                        match = classesToCheck[m].test(node.className);
                                        if (!match) break;
                                    }
                                    if (match) returnElements.push(node);
                                }
                            }
                            return returnElements;
                        }
                    }
                </script>';
            }
        }


    }

    /**
     * Formats backtrace array into an HTML string
     *
     * @param $backtrace_array
     *
     * @return string Formatted as HTML
     */
    public static function getBacktraceHTML($backtrace_array)
    {
        $table = '
            <table class="backtrace">
                <tr>
                    <th colspan="5" align="left">Stack Trace</th>
                </tr>';
        $i = 0;
        foreach ($backtrace_array as $backtrace)
        {
            if (isset($backtrace['class']) && $backtrace['class'] == 'ErrorSet')
            {
                continue;
            }
            $args = '';
            if (isset($backtrace['args']))
            {
                for ($j = 0; $j < count($backtrace['args']); $j++)
                {
                    $args .= ($j != 0 ? ', ' : '');
                    if (is_string($backtrace['args'][$j]))
                    {
                        $args .= '<span style="color: #0000AA">"' . htmlentities($backtrace['args'][$j]) . '"</span>';
                    }
                    else if (is_numeric($backtrace['args'][$j]))
                    {
                        $args .= '<span style="color: #009900">' . $backtrace['args'][$j] . '</span>';
                    }
                    else if (is_object($backtrace['args'][$j]))
                    {
                        $args .= '<span  style="cursor: pointer" onclick="this.childNodes[1].style.display=(this.childNodes[1].style.display==\'none\'?\'\':\'none\')">Object...<span style="display: none">' . nl2br(htmlentities(print_r($backtrace['args'][$j], true))) . '</span></span>';
                    }
                    else if (is_array($backtrace['args'][$j]))
                    {
                        $args .= '<span  style="cursor: pointer" onclick="this.childNodes[1].style.display=(this.childNodes[1].style.display==\'none\'?\'\':\'none\')">Array...<span style="display: none"><pre>' . htmlentities(print_r($backtrace['args'][$j], true)) . '</pre></span></span>';
                    }
                    else
                    {
                        ob_start();
                        var_dump($backtrace['args'][$j]);
                        $string = ob_get_clean() . "\n";
                        $args .= htmlentities($string);
                    }
                }
            }
            $table .= '
                <tr class="' . ($i % 2 == 0 ? 'light' : 'dark') . '">
                    <td>#' . $i . '</td>
                    <td>' . (isset($backtrace['class']) ? $backtrace['class'] . $backtrace['type'] : '') . $backtrace['function'] . '(' . $args . ')</td>
                    <td>' . (isset($backtrace['file']) ? $backtrace['file'] : '') . '</td>
                    <td>' . (isset($backtrace['line']) ? $backtrace['line'] : '') . '</td>
                    <td>' . (isset($backtrace['object']) ? '<span style="cursor: pointer" onclick="this.childNodes[1].style.display=(this.childNodes[1].style.display==\'none\'?\'\':\'none\')">Object...<span style="display: none"><pre>' . print_r($backtrace['object'], true) : '') . '</pre></span></span></td>
                </tr>';
            $i++;
        }

        $table .= '</table>';
        return $table;
    }

    public static function sendEmail()
    {
        $content = '';
        if (self::$email !== NULL && (count(self::$errors) > 0 || !Console::isEmpty()))
        {
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: errors@elykinnovation.com' . "\r\n";

            if(self::$email_format==self::EMAIL_FORMAT_NORMAL)
            {
                if (strtolower($_SERVER['REQUEST_METHOD']) == 'post')
                {
                    Console::add($_POST);
                }

                $content =
                    '<p>
                        <b>Errors (' . count(self::$errors) . '):</b><br />
                        ' . self::getErrorHTML() . '
                    </p>
                    <p>
                        <b>Console (' . Console::size() . '):</b><br />
                        <pre>' . Console::toString() . '</pre>
                    </p>
                    <p>
                        <b>Query Log (' . QueryLog::size() . '):</b><br />
                        <pre>' . QueryLog::toString() . '</pre>
                    </p>';
            }
            else if(self::$email_format == self::EMAIL_FORMAT_JSON)
            {
                $json = array
                (
                    'request_info'=>self::getHeaderJSON(),
                    'errors'=>self::getErrorJSON(),
                    'console'=>Console::getJSON(),
                    'sql'=>QueryLog::getJSON(),
                    'post'=>$_POST,
                    'get'=>$_GET,
                    'server'=>$_SERVER
                );
                if(!empty($_SESSION))
                {
                    $json['session'] = $_SESSION;
                }
                $content = base64_encode(json_encode($json, JSON_PARTIAL_OUTPUT_ON_ERROR));
            }

            if (self::$email_subject === NULL)
            {
                if(self::$email_format==self::EMAIL_FORMAT_NORMAL)
                {
                    self::$email_subject = 'Error Report: ' . (empty($_SERVER['HTTP_HOST']) ? 'CLI Script' : $_SERVER['HTTP_HOST']) . ' [' . count(self::$errors) . ', ' . Console::size() . '] - ' . md5(json_encode(self::$errors) . Console::toString());
                }
                else if(self::$email_format==self::EMAIL_FORMAT_JSON)
                {
                    self::$email_subject = 'JSON Error Report: ' . (empty($_SERVER['HTTP_HOST']) ? 'CLI Script' : $_SERVER['HTTP_HOST']);
                }
            }

            $mail_sent = false;
            if (self::$encrypt_email)
            {
                $temp_file = tempnam(sys_get_temp_dir(), 'err');
                $temp_file_encrypted = tempnam(sys_get_temp_dir(), 'enc');
                file_put_contents($temp_file, $headers . "\r\n" . $content);

                if (openssl_pkcs7_encrypt($temp_file, $temp_file_encrypted, self::$email_encryption_key, array('From' => 'errors@elykinnovation.com'), 0, 1))
                {
                    $parts = preg_split('/\r?\n\r?\n/', file_get_contents($temp_file_encrypted), 2);
                    self::$email_success = mail(self::$email, self::$email_subject, $parts[1], $parts[0]);
                    $mail_sent = true;
                }
                unlink($temp_file);
                unlink($temp_file_encrypted);
            }

            if (!$mail_sent)
            {
                self::$email_success = mail(self::$email, self::$email_subject, $content, 'From: errors@elykinnovation.com' . "\r\n");
            }
        }
    }
}

/**
 * Class Console
 *
 * Provides a replacement for print-debugging.
 */
class Console
{
    private static $array = array();
    private static $spritesheet = 'iVBORw0KGgoAAAANSUhEUgAAAEIAAAAQCAYAAACiEqkUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjExR/NCNwAAANJJREFUWEftk9ENwjAMRD0Nc2SMjsEGDMQWmYIFmCP4olxlElv8EbXNkx5ym+DEJ5Ci3J9buT2kinpn01p0C0RN3lq/9B1ErdgeX73sXvsd29s7489IOEB00WCwvgcd9tK+N52EeJeH7iWhN5Tq9YDe3qrXG05iBdFYf41GPdmGUUMgNgzUxIaBWgkDDYILg56Enr4ANYiUUijx1ugZEAyScw7lsN4aPUMYQxDe8wqiPV8qCA5ste9/7Tk66xfRWEE0hiB6LxMEPjisJ/HW6PEp5QPj6zlNioJYDwAAAABJRU5ErkJggg==';

    /**
     *
     */
    public static function add()
    {
        $bk = debug_backtrace();
        for($i=0; $i<func_num_args(); $i++)
        {
            $j = count(self::$array);
            self::$array[$j]['item'] = func_get_arg($i);
            self::$array[$j]['file'] = $bk[0]['file'];
            self::$array[$j]['line'] = $bk[0]['line'];
        }
    }

    public static function getJSON()
    {
        return self::$array;
    }
    
    /**
     * @return string
     */
    public static function toString()
    {
        $string = '<style type="text/css">
		.ErrorSet_console .ErrorSet_bullet_green,
		.ErrorSet_console .ErrorSet_bullet_red,
		.ErrorSet_console .ErrorSet_bullet_yellow,
		.ErrorSet_console .ErrorSet_bullet_green_square,
		.ErrorSet_console .ErrorSet_bullet_red_square,
		.ErrorSet_console .ErrorSet_bullet_yellow_square,
		.ErrorSet_console .ErrorSet_plus,
		.ErrorSet_console .ErrorSet_minus {
			background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEIAAAAQCAYAAACiEqkUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjExR/NCNwAAANdJREFUWEftlNENwyAMRJmmc2SzjsMeTJFtqG1sgpxDzRcSgZPeR1z3cmepDazPN0QiK1GGpBxCJLJyzU+anzQryNx5VC+3W7/jvG/vGC4O68KXAp2gUgIUAx4C2C3cvQWNNV4oPOMDVlApAnkwaFdA3oTGGi8UnvEBK6gUgTwYtCsgb0JjjReF3T8NEwVui6z5Z7nV6DiO3ENXHu1MLS6SUupiZdFnxiuO4Uui530IfV7qEFa4pZ3/21G7eWVlDPTsZ559COWVh/AscwiWlUXoyqOdeRXCD4TIOIxQ6zvPAAAAAElFTkSuQmCC);
			width: 6px;
			height: 6px;
			float: left;
			margin: 5px 3px 0 0;
		}

		.ErrorSet_console .ErrorSet_plus,
		.ErrorSet_console .ErrorSet_minus {
			width: 9px;
			height: 9px;
			margin: 3px 3px 0 0;
			cursor: pointer;
		}

		.ErrorSet_console .ErrorSet_plus {
			background-position: 0 -7px;
		}

		.ErrorSet_console .ErrorSet_minus {
			background-position: -9px -7px;
		}

		.ErrorSet_console .ErrorSet_bullet_green {
			background-position: 0 0;
		}

		.ErrorSet_console .ErrorSet_bullet_red {
			background-position: -6px 0;
		}

		.ErrorSet_console .ErrorSet_bullet_yellow {
			background-position: -12px 0;
		}

		.ErrorSet_console .ErrorSet_bullet_green_square {
			background-position: -18px 0;
		}

		.ErrorSet_console .ErrorSet_bullet_red_square {
			background-position: -24px 0;
		}

		.ErrorSet_console .ErrorSet_bullet_yellow_square {
			background-position: -30px 0;
		}

		.ErrorSet_type_string {
			color: #aa0000;
		}

		.ErrorSet_type_int {
			color: #00aa00;
		}

		.ErrorSet_type_bool {
			color: #0000aa;
		}

		.ErrorSet_type_resource {
			color: #FF6600;
		}
		</style>';
        for($i = 0; $i<count(self::$array); $i++)
        {
            $string .= '<p><pre>From '.self::$array[$i]['file']. ' on line '.self::$array[$i]['line']."</pre>\n";

            if(is_object(self::$array[$i]['item']))
                $string .= self::displayObject(self::$array[$i]['item']);
            else if(is_array(self::$array[$i]['item']))
                $string .= self::displayArray(self::$array[$i]['item']);
            else
                $string .= self::getTypeSpan(self::$array[$i]['item']);

            $string.='</p>';
        }

        return $string;
    }

    public static function addToAlertSet()
    {
        for($i=0; $i<count(self::$array); $i++)
            AlertSet::addDebug(self::$array[$i]['file'].': '.self::$array[$i]['line'].' -- '.self::$array[$i]['item']);
    }

    private static function displayObject($object, $depth=0, $objects_seen=array())
    {
        foreach($objects_seen as $obj)
        {
            if($obj===$object)
            {
                return '***Recursion***';
            }
        }
        //return print_r($object, true);
        $rand_string = randString(16);
        $str = '';
        $reflector = new ReflectionClass($object);
        $str .= '<div class="ErrorSet_minus" onclick="document.getElementById(\'ErrorSet_'.$rand_string.'\').style.display=(this.className==\'ErrorSet_plus\'?\'block\':\'none\');document.getElementById(\'ErrorSet_'.$rand_string.'_break\').style.display=(this.className==\'ErrorSet_plus\'?\'block\':\'none\');document.getElementById(\'ErrorSet_'.$rand_string.'_ellipses\').style.display=(this.className==\'ErrorSet_plus\'?\'none\':\'\'); this.className=(this.className==\'ErrorSet_plus\'?\'ErrorSet_minus\':\'ErrorSet_plus\')"></div>'.get_class($object).'<div id="ErrorSet_'.$rand_string.'_break"></div> {
		<span id="ErrorSet_'.$rand_string.'_ellipses" style="display: none; margin: 0 6px 0 0;">...</span><div id="ErrorSet_'.$rand_string.'" style="margin-left: 2px; padding-left: '.(20*($depth+1)).'px; border-left: 1px solid #bababa;">';
        foreach($reflector->getProperties() as $property)
        {
            $bullet = 'ErrorSet_bullet_';
            if($property->isPublic())
                $bullet .= 'green';
            else if($property->isPrivate())
            {
                $property->setAccessible(true);
                $bullet .= 'red';
            }
            else if($property->isProtected())
            {
                $property->setAccessible(true);
                $bullet .= 'yellow';
            }

            if($property->isStatic())
                $bullet .= '_square';

            $str.='<div class="name"><div class="'.$bullet.'"></div>'.$property->getName().' : ';
            $value = $property->getValue($object);
            if(is_object($value))
            {
                $objects_seen[] = $object;
                $str.=self::displayObject($value, $depth, $objects_seen).', ';
            }
            else if(is_array($value))
                $str.=self::displayArray($value, $depth).', ';
            else
                $str.=self::getTypespan($value).',';

            $str .= '</div>';
        }
        $str .= '</div>}';
        return $str;
        //return print_r($object, true);
    }

    private static function displayArray($array, $depth=0)
    {
        //return var_export($array, true);
        $rand_string = randString(16);
        $str = '';
        if(!empty($array))
        {
            $str .= '<div class="ErrorSet_minus" onclick="document.getElementById(\'ErrorSet_'.$rand_string.'\').style.display=(this.className==\'ErrorSet_plus\'?\'block\':\'none\');document.getElementById(\'ErrorSet_'.$rand_string.'_break\').style.display=(this.className==\'ErrorSet_plus\'?\'block\':\'none\');document.getElementById(\'ErrorSet_'.$rand_string.'_ellipses\').style.display=(this.className==\'ErrorSet_plus\'?\'none\':\'\'); this.className=(this.className==\'ErrorSet_plus\'?\'ErrorSet_minus\':\'ErrorSet_plus\')"></div>array<div id="ErrorSet_'.$rand_string.'_break"></div> [ ';
        }
        else
            $str .= 'array [ ';
        $str .='<span id="ErrorSet_'.$rand_string.'_ellipses" style="display: none; margin: 0 6px 0 0;">...</span>';
        if(!empty($array))
        {
            $str.='<div id="ErrorSet_'.$rand_string.'" style="margin-left: 2px; padding-left: '.(20*($depth+1)).'px; border-left: 1px solid #bababa;">';
            foreach($array as $key=>$value)
            {
                $str.='<div>'.self::getTypeSpan($key).' => ';
                if(is_object($value))
                    $str.=self::displayObject($value, $depth).', ';
                else if(is_array($value))
                    $str.=self::displayArray($value, $depth).', ';
                else
                    $str.=self::getTypeSpan($value).', ';

                $str .='</div>';
            }
            $str .= '</div>';
        }
        $str .= ']';
        return $str;
    }

    /**
     * @param $value
     *
     * @return string
     */
    private static function getTypeSpan($value)
    {
        $type = 'string';
        if(is_bool($value))
        {
            $type = 'bool';
            $value = $value?'TRUE':'FALSE';
        }
        else if(is_resource($value))
            $type = 'resource';
        else if(is_int($value) || is_double($value) || is_float($value) || is_long($value))
            $type = 'int';
        else if(is_string($value))
        {
            $type = 'string';
        }
        else if(is_null($value))
        {
            $type = 'bool';
            $value = 'NULL';
        }

        return '<span class="ErrorSet_type_'.$type.'">'.$value.'</span>';
    }

    /**
     * @return bool
     */
    public static function isEmpty()
    {
        return (count(self::$array)==0);
    }

    /**
     * @return int
     */
    public static function size()
    {
        return count(self::$array);
    }
}

/**
 * Class QueryLog
 */
class QueryLog
{
    private static $array = array();

    /**
     *
     */
    public static function add()
    {
        for ($i = 0; $i < func_num_args(); $i++)
            self::$array[] = func_get_arg($i);
    }

    public static function getJSON()
    {
        return self::$array;
    }

    /**
     * @return string
     */
    public static function toString()
    {
        $string = '';
        for ($i = 0; $i < count(self::$array); $i++) {
            $string .= '<p><pre>' . htmlentities(
                    preg_replace('/[\n\s]+$/', '',
                        preg_replace('/\t+/', "\t",
                            preg_replace('/(^|\n)\t*(SELECT|FROM|LEFT JOIN|OUTER JOIN|INNER JOIN|WHERE|ORDER BY|GROUP BY|LIMIT)[\n\s]*/', "$1$2\n\t",
                                self::$array[$i])))) . '</pre></p>' . ($i == count(self::$array) - 1 ? '' : "<hr />\n");
        }

        return $string;
    }

    /**
     * @return bool
     */
    public static function isEmpty()
    {
        return (count(self::$array) == 0);
    }

    /**
     * @return int
     */
    public static function size()
    {
        return count(self::$array);
    }

    public static function addToAlertSet()
    {
        if(ErrorSet::$display)
        {
            for($i=0; $i<count(self::$array); $i++)
                AlertSet::addSQL('<pre>'.htmlentities(
                        preg_replace('/[\n\s]+$/', '',
                            preg_replace('/\t+/', "\t",
                                preg_replace('/(^|\n)\t*(SELECT|FROM|LEFT JOIN|OUTER JOIN|INNER JOIN|WHERE|ORDER BY|GROUP BY|LIMIT)[\n\s]*/', "$1$2\n\t",
                                    self::$array[$i])))).'</pre>');
        }
    }
}

ErrorSet::$start_timestamp = microtime(true);

ErrorSet::$old_handler = set_error_handler(array('ErrorSet', 'error_handler'));
register_shutdown_function(array('ErrorSet', 'shutdown_function'));
?>
