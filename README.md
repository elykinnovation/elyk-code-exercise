# README #

### What is this repository for? ###

This repository is a selection of code taken from the platform that eLYK Innovation developers use on a daily basis.
Its purpose is to allow eLYK Innovation to gauge the skill of prospective developers. In the following sections of this
document, a scenario will be set up for the prospective candidate with tasks to complete and submit to eLYK Innovation.

### How do I get set up? ###

1. Clone this repository. The DOCUMENT_ROOT of your development web server should be the `www` directory of this repo.
1. Create a MySQL database to use for this project.
1. Import `database/1.sql` into your database.
1. Import `database/data.sql` into your database.
1. Copy `private/config-env.sample.xml` and name it `private/config-env-production.xml`.
1. Open `private/config-env-production.xml` and:
    * replace all instances of `[PROJECT_ROOT]` to the absolute path of the repo. (eg. if you cloned the repo to `d:\projects\elyk_test`, replace `[PROJECT_ROOT]` with `d:\projects\elyk_test`).
    * replace all `[DB_*]` instances to the applicable database values.
1. You should now have a development environment successfully configured.
1. The default username is `dev_user` and the password is `elykTest`


### How does our platform work? ###
Check the [wiki](http://www.bitbucket.org/elykinnovation/elyk-code-exercise/wiki)!

### The Exercise

Read through the wiki to get an understanding of how our platform works.

Once you are finished, get the platform set up on your environment of choice. Expect to spend some time debugging in order for the platform to run locally; this is part of the exercise. Once you get it working so you can add/edit/delete users, read through the following scenario and complete it. Prepare a zip file of the completed project along with a dump of the MySQL database to deliver.

#### Scenario

The client requires a job application module. Prospective candidates send resumes to the office manager via email. The admin user creates a User for the candidate in the Users module. The admin will then create a job application in the job applications module. The module should have the ability to fill out a job application with the following fields:

* User (select from all non-Admin users from the existing `Users` module)
* Position desired [text]
* Cover letter [textarea]
* Availability date [date]
* Skills [textarea]
* Resume upload [file upload]

Admin users should be able to view, add, edit, and delete all job applications. On the job application listing page, users should be able to search by candidate first name, last name, or skills and see all job applications that match the search criteria.

For simplicity, there is no need for the candidate users to be able to log in to the system.