<?php
include($_SERVER['DOCUMENT_ROOT'].'\private\includes\config.inc.php');

$showForm = true;
if( @$_POST['verb'] == 'forgotpw' )
{
	if(!empty($_POST['email']))
    {
		if(Users::sendforgotPasswordLink($_POST['email']))
		{
			//$json['success'] = true;
			$msg = ('You have been sent an email with further instructions. Please follow the link in the email to reset your password. If the email does not appear in your inbox, please check any spam folders. ');
			$showForm = false;
		}
		else
			$msg = ('We were not able to send the email to the specified address. Please make sure that the email address you are using is the same address you have provided and try again.');
	}
	else
		$msg = ('You must enter your email address.');
}
else if( @$_POST['verb'] == 'resetpw' )
{
	if($_POST['password'] == $_POST['confirm_password'] && !empty($_POST['password']))
		{
			if(Users::setPassword($_POST['key'], $_POST['password']))
			{
				//$json['success'] = true;
				$msg = ('Your password has been changed. You may now <a href="/index.php" title="Log In">log in.</a>');
			}
			else
				$msg = ('There was an error changing your password.');
		}
		else
			$msg = ('Passwords do not match.');	
}
?>
	
   <?php
   	if( !isset($_GET['key'] ) ){ ?>
		<h1>Forgot Password</h1>
    <p>Forgot your password? Enter your email address that you used to apply for The Last Fan below. </p>
    <p><strong><?php echo $msg ?></strong></p>
    
    <?php echo XSRF::html()?>
<form action="" method="post"  enctype="multipart/form-data">
	<input type="hidden" name="verb" value="forgotpw" />
	<table class="info_table">
    	<tr>
        	<td>
            	<?php echo AlertSet::html()?>
            </td>
        </tr>
       <?php

	   if($showForm)
       {
		   ?>
		   <tr>
				<th align="left">Email:</th>
				<td><input type="text" name="email" /></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Submit" class="button" /></td>
			</tr> 
    		<?php

		} 
		?>
	</table>
	</form>
    <?php
}
	else {
	?>
    	<?php echo  (isset($_GET['setpw']))  ? '<h1>Set your password</h1>' : '<h1>Reset your password</h1>' ?>
    <p><strong><?php echo @$msg ?></strong></p>
<form action="" method="post"  enctype="multipart/form-data">
			<input type="hidden" name="verb" value="resetpw" />
			<table>
				<tr>
					<td>
						<label for="password"><strong>New Password:</strong></label>
					</td>
					<td>
						<input type="password" name="password" id="password" />
					</td>
				</tr>
				<tr>
					<td>
						<label for="confirm"><strong>Confirm Password:</strong></label>
					</td>
					<td>
						<input type="password" name="confirm_password" id="confirm" />
					</td>
				</tr>
				<tr>
					<td>
						<input type="hidden" name="key" id="key" value="<?php echo htmlentitiesUTF8($_GET['key'])?>" />
						<input type="submit" value="Submit" />
					</td>
				</tr>
			</table>
		</form>
    <?php
	}
	?>
	




