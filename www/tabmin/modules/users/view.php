<?php

ini_set('display_errors', true);
include($_SERVER['DOCUMENT_ROOT'].'/../private/includes/config.inc.php');
include INCLUDE_ROOT.'/secure.inc.php';
Breadcrumbs::push('Users', '/admin/users');
TemplateSet::begin('body');

switch(@$_GET['sort'])
{
	case 'last_name':
		$sort='users.last_name '. (@$_GET['sort_order']=='desc'? 'DESC' : '') .', users.first_name';
	break;
	case 'email':
		$sort = 'users.email '. (@$_GET['sort_order']=='desc'? 'DESC' : '');
	break;	
	case 'role':
	default:
		$sort='users.roles_id '. (@$_GET['sort_order']=='desc'? 'DESC' : '') .', users.last_name, users.first_name, users.email';
	break;
}
?>
<p>Search users by first and last names, email, phone:</p>
<?php

	$roles = Roles::loadAll();
?>

    <form method="get" action="">
        <input type="text" name="s" value="<?php echo @$_GET['s'] ?>" />

        <select name="roles_id">
            <option  value="">Please Select Role</option>
            <?php

            foreach ($roles as $role)
            {
                ?>
                <option value="<?php echo $role->getId()?>" <?php echo (@$_GET['roles_id'] == $role->getId()) ? 'selected' : ''?>><?php echo $role->getRole()?></option>
            <?php

            }
            ?>
        </select>
        <input type="submit" class="btn btn-primary" value="Search" />
        <input type="submit" class="btn btn-danger" value="Clear Search" onclick="this.form.elements['s'].value=''; this.form.elements['roles_id'].value=''" />
    </form>
<?php


$page=intval(@$_GET['page']);
$records_per_page=50;
if($page<=0)
	$page=1;

$parameters = array();
if (@$_GET['roles_id'])
	$parameters['roles.id'] = array('type'=>'int', 'condition'=>'=', 'value'=>$_GET['roles_id']);

$users=Users::searchLoadByParameters(@$_GET['s'], $parameters, $sort, (($page-1)*$records_per_page).','.$records_per_page);
	
$total_records=Users::getFoundRows();
if($total_records > 0)
{
	$total_pages=ceil($total_records/$records_per_page);
	?>
	<table class="info_table">
		<tr>
			<th colspan="7" align="left">
				<div style="float:right">
                    <?php

                    if($page>1)
                    {
                        echo '<div class="action_button"><a href="?page=1&s='.P::out(@$_GET['s']).'"><img src="'. $module->getIcon('page_first') .'" /></a></div>';
                        echo '<div class="action_button"><a href="?page='.($page-1).'&s='.P::out(@$_GET['s']).'"><img src="'. $module->getIcon('page_previous') .'" /></a></div>';
                    }

                    echo ' <div style="float:left;">Page <select onchange="document.location.href=\'?page=\'+this.value+\'s='.P::out(@$_GET['s']).'\'">';
                    for($i=1; $i<=$total_pages; $i++)
                    {
                        echo '<option value="'.$i.'"';
                        if($page==$i)
                            echo ' selected="selected"';
                        echo '>'.$i.'</option>';
                    }
                    echo '</select></div> ';

                    if($page<$total_pages)
                    {
                        echo '<div class="action_button"><a href="?page='.($page+1).'&s='.P::out(@$_GET['s']).'"><img src="'. $module->getIcon('page_next') .'" /></a></div>';
                        echo '<div class="action_button"><a href="page='.$total_pages.'&s='.P::out(@$_GET['s']).'"><img src="'. $module->getIcon('page_last') .'" /></a></div>';
                    }
                    ?>
				</div>
				Displaying <?php echo count($users)?> of <?php echo $total_records?> total users.
			</th>
		</tr>
		<tr>
			<th>Name</th>
			<th>Email</th>
            <th>Role</th>
			<th width="80">Actions</th>
		</tr>
		<?php

		foreach($users as $i=>$user)
		{
			$class='light';
			if($i%2==0)
				$class='dark';
			?>
			<tr class="<?php echo $class ?>">
				<td>
					<?php P::out($user->getLastName() .', '. $user->getFirstName()); ?>
                </td>
				<td><?php P::out($user->getEmail()) ?></td>
                <td><?php P::out($user->getRole()->getRole())?></td>
				<td>
					<div class="action_button" title="Edit User">
						<?php

						if($currentUser->hasPermission($module, 'edit', $currentUser->getId()==$user->getId()))
						{
							?>
							<a href="/admin/users/edit/<?php echo $user->getId()?>"><img src="<?php echo $module->getIcon('edit') ?>" /></a>
							<?php

						}
						?>
					</div>
					<div class="action_button" title="Delete User">
						<?php
						if($currentUser->hasPermission($module, 'delete', $currentUser->getId()==$user->getId()))
						{
							?>
							<form action="<?php echo $module->path ?>/ajax.php" method="post" onsubmit="AlertSet.confirm('Are you sure you want to delete this user?', function(){Ajax.submit(this, function(){document.location.reload()}, function(resp){AlertSet.clear().addJSON(resp).show();});}.bind(this)); return false;">
								<input type="hidden" name="verb" value="delete" />
								<input type="hidden" name="users_id" value="<?php echo $user->getId() ?>" />
								<?php echo XSRF::html() ?>
								<input type="image" src="<?php echo $module->getIcon('delete')?>" />
							</form>
							<?php

						}
						?>
					</div>
                    
				</td>
			</tr>
			<?php

		}
		?>
	</table>
	<?php
}
else
	echo '<br /><strong>There are currently no users in the database.</strong>';
TemplateSet::end();
TemplateSet::display('../../../template.php');
?>