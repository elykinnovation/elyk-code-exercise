<?php

include($_SERVER['DOCUMENT_ROOT'].'/../private/includes/config.inc.php');
if(@$_POST['verb']!='forgotpw' && @$_POST['verb']!='resetpw' && @$_POST['verb']!='signup')
	include INCLUDE_ROOT.'/ajax_secure.inc.php';
ErrorSet::setAJAX(true);
$properties = array
				(
					'id'=>$_POST['users_id'],
					'role'=>array
					(
						'id'=>@$_POST['roles_id']
					),
					'email'=>$_POST['email'],
					'first_name'=>$_POST['first_name'],
					'last_name'=>$_POST['last_name'],
					'password'=>$_POST['password'],
					'password2'=>$_POST['password2'],
					'phone'=>$_POST['phone']
				);

$controller = new UserController($properties, $currentUser, $module);

$controller->setAllRequired(array(
		'email'=>'Email',
		'first_name'=>'First Name',
		'last_name'=>'Last Name',
		'password'=>'Password',
		'password2'=>'Repeated Password'
		));

//Console::add(var_export($_POST, true));
$module = 'users';
switch(@$_POST['verb'])
{
	case 'add':			
		$controller->add();
	break;
	case 'edit':		
		$controller->removeRequired('password');
		$controller->removeRequired('password2');	
		$controller->update();
	break;
	case 'delete':			
		$controller->delete();
	break;
	case 'sign-up':			
		$controller->setCheckPermissions(false);		
		$controller->add();
	break;
	case 'forgotpw':			
		$controller->forgotPassword();
	break;
	case 'resetpw':
		$controller->addProperty('key', $_POST['key']);			
		$controller->resetPassword();
	break;
}


echo $controller->getJSON();
?>
