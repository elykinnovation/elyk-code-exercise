<form action="<?php echo $module->path?>/ajax.php" method="post" class="tabmin_form" onsubmit="return Ajax.submit(this, function(resp){AlertSet.addJSON(resp).show(); AlertSet.onClose = function(){document.location.href='/admin/users'}}, function(resp) {AlertSet.addJSON(resp).show();})" autocomplete="off" enctype="multipart/form-data">
	<input type="hidden" name="verb" value="<?php echo $verb?>" />
	<input type="hidden" name="users_id" value="<?php echo (@$user ? $user->getId() : '')?>" />
	<?php echo XSRF::html()?>
	<table class="info_table" style="width: auto;">
		<tr>
			<th align="left" >Role:</th>
			<td>
				<select name="roles_id">
					<?php

					$roles = Roles::loadAll();
					$options='';
					foreach($roles as $i=>$role)
					{
						$options .= '<option value="'.$role->getId().'"';
						if(!empty($user) && $user->getRole()->getId()==$role->getId() || empty($user) && $role->getId()==3)
							$options .= ' selected';
						$options .= '>'.P::sanitize($role->getRole()).'</option>';
					}
					echo $options;
					?>
				</select>
			</td>
		</tr>
		<tr>
			<th align="left">Email:</th>
			<td><input type="text" name="email" value="<?php P::out(@$user ? $user->getEmail() : '')?>" />
				<small class="admin_instruction">Users will log in with their email address.</small></td>
		</tr>
		<tr>
			<th align="left">Password:</th>
			<td>
				<input type="password" name="password" />
				<?php

				if($verb=='edit' || $verb=='member_edit')
					echo '<small><em>Leave blank to keep the existing password.</em></small>';
				?>
			</td>
		</tr>
		<tr>
			<th align="left">Password&nbsp;Again:</th>
			<td><input type="password" name="password2" /></td>
		</tr>
		<tr>
			<th align="left">First Name:</th>
			<td><input type="text" name="first_name" value="<?php P::out(@$user ? $user->getFirstName() : '')?>" /></td>
		</tr>
		<tr>
			<th align="left">Last Name:</th>
			<td><input type="text" name="last_name" value="<?php P::out(@$user ? $user->getLastName() : '')?>" /></td>
		</tr>
        <tr>
			<th align="left">Phone Number:</th>
			<td><input type="text" name="phone" value="<?php P::out(@$user ? $user->getPhone() : '')?>" /></td>
		</tr>
		<tr>
			<td>
				<input type="button" class="btn btn-danger" value="Cancel" onclick="AlertSet.confirm('Are you sure? You will lose any unsaved data.', function(){document.location.href='/admin/users'})" />
			</td>
			<td><input type="submit" class="btn btn-primary" value="Save" /></td>
		</tr>
	</table>
</form>
