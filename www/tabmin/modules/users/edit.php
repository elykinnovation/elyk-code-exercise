<?php

include($_SERVER['DOCUMENT_ROOT'].'/../private/includes/config.inc.php');
include INCLUDE_ROOT.'/secure.inc.php';
TemplateSet::begin('body');

$verb='edit';
if(!empty($_GET['users_id']))
{
	$user=Users::loadById($_GET['users_id']);
}

if($user)
{
    include 'form.php';
    Breadcrumbs::push('Users', '/admin/users');
    Breadcrumbs::push('Edit');
    Breadcrumbs::push($user->getFirstName().' '.$user->getLastName());
}
else
	echo 'The user you selected was not found in the database. They may have been deleted.';

TemplateSet::end();
TemplateSet::display('../../../template.php');
?>