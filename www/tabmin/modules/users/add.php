<?php

ini_set('display_errors', true);
include($_SERVER['DOCUMENT_ROOT'].'/../private/includes/config.inc.php');
include INCLUDE_ROOT.'/secure.inc.php';
TemplateSet::begin('body');
Breadcrumbs::push('Users', '/admin/users');
Breadcrumbs::push('Add');
$verb='add';
include 'form.php';
TemplateSet::end();
TemplateSet::display('../../../template.php');
?>