<?php
ini_set('display_errors', 1);
include($_SERVER['DOCUMENT_ROOT'].'/../private/includes/config.inc.php');
TemplateSet::begin('body');
?>
	<div style="margin:15px 50px 0;">
		<?php

		if(!empty($currentUser))
		{
            Breadcrumbs::push('Home', '/');
        ?>

        <?php
		}
		else
		{
			?>
			<h1><?php echo SITE_NAME?> Admin</h1>
			<?php echo AlertSet::html()?>
			<div style="width:200px; margin:80px auto 0;">
				<form method="post" action="index.php" class="tabmin_form">
					<input type="hidden" name="verb" value="login" />
					<?php echo XSRF::html()?>
					<table>
						<tr>
							<th colspan="2">Log In</th>
						</tr>
						<tr>
							<td><label for="email">Email:</label></td>
							<td><input type="text" name="email" id="email" /></td>
						</tr>
						<tr>
							<td><label for="password">Password:</label></td>
							<td><input type="password" name="password" id="password" /></td>
						</tr>
						<tr>
							<td></td>
							<td><input type="submit" value="Login" /></td>
						</tr>
					</table>
				</form>
			</div>
			<?php
		}
		?>
	</div>
<?php
TemplateSet::end();
TemplateSet::display(DEFAULT_TEMPLATE);
?>