<?php
global $currentUser, $modules, $module, $tab ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="description" />
	<meta name="keywords" content="keywords" />
	<meta name="owner" content="eLYK Innovation" />
	<meta name="author" content="eLYK Innovation" />
	<meta http-equiv="charset" content="ISO-8859-1" />
	<meta http-equiv="content-language" content="english" />
	<meta name="rating" content="general" />
	
	<title><?php TemplateSet::begin('title')?><?php echo SITE_NAME ?><?php TemplateSet::end()?></title>

	<link rel="stylesheet" type="text/css" href="/css/tabmin.css" />
	<link rel="stylesheet" type="text/css" href="/css/global.css" />
	<link rel="stylesheet" type="text/css" href="/css/AlertSet.css" />
	<script src="/js/util.js" type="text/javascript"></script>
	<script src="/js/Ajax.js" type="text/javascript"></script>
    <script src="/js/json2.js" type="text/javascript"></script>
    <script src="/js/AlertSet.js" type="text/javascript"></script>
    <script src="/js/Tabmin.js" type="text/javascript"></script>
</head>

<body>

<header>
    <div class="wrapper">
        <div class="logo">
            <a href="/"><h1>eLYK Test</h1></a>
        </div>

    </div>
    <nav>
        <div class="wrapper">
            <ul>
                <?php
                if(!empty($currentUser))
                {
                    if(!empty($modules))
                    {
                        foreach($modules as $a_module)
                        {
                            if($currentUser->tabPermission($a_module, 'view'))
                            {
                                ?>
                                <li><a href="/admin/<?php P::out($a_module->name) ?>"<?php echo !empty($module) && $a_module->name==$module->name ? ' class="selected"' : '' ?>><?php P::out($a_module->title) ?></a>
                                    <?php
                                    if(!empty($a_module->tabs))
                                    {
                                        ?>
                                        <ul class="dropdown">
                                            <?php
                                            foreach($a_module->tabs as $a_tab)
                                            {
                                                if($a_tab->hidden)
                                                {
                                                    continue;
                                                }
                                                ?>
                                                <li>
                                                    <a href="/admin/<?php P::out($a_module->name) ?>/<?php P::out($a_tab->name) ?>">
                                                        <div style="float: left">
                                                            <img style="margin-top: 2px; margin-right: 2px" src="<?php echo $a_module->getIcon($a_tab->name)?>" alt="<?php P::out($a_tab->title) ?>"/>
                                                        </div>
                                                        <div style="float: left">
                                                            <?php P::out($a_tab->title) ?>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </a>
                                                </li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                        <?php

                                    }
                                    ?>
                                </li>
                                <?php
                            }
                        }
                    }
                }
                else
                {
                    ?>
                    <li><a href="/" title="Home">Home</a></li>
                    <?php
                }
                ?>
            </ul>
            <?php
            if(!empty($currentUser))
            {
                ?>
                <div class="welcome">Hi, <?php P::out($currentUser->getFirstName()) ?>. [ <a href="?logout" title="Log Out">Log Out</a> ]</div>
            <?php
            }
            ?>
        </div>
    </nav>
</header>

<div class="clear"></div>
<div class="breadcrumbs wrapper">
    <ul>
        <li class="home"><a href="/"></a></li>
        <?php Breadcrumbs::out(); ?>
    </ul>
</div>
<div class="clear"></div>
<div class="content-wrapper wrapper">
    <section class="content">
        <?php TemplateSet::begin('sidebar') ?><?php TemplateSet::end() ?>
        <?php TemplateSet::begin('body') ?><?php TemplateSet::end() ?>
        <div class="clear"></div>
    </section>
</div>
<footer></footer>
<script type="text/javascript">
    Tabmin.appendTooltips();
</script>

</body>
</html>
