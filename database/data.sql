-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2015 at 05:41 PM
-- Server version: 5.6.17
-- PHP Version: 5.4.42

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `elyk_test`
--

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `roles_id`, `module`, `tab`, `verb`, `own`) VALUES
(4, 1, 'users', 'view', NULL, NULL),
(5, 1, 'users', 'add', 'add', NULL),
(6, 1, 'users', 'edit', 'edit', NULL),
(7, 1, 'users', NULL, 'delete', 0),
(24, 2, 'users', 'view', NULL, NULL),
(25, 2, 'users', 'add', 'add', NULL),
(26, 2, 'users', NULL, 'delete', NULL),
(27, 2, 'users', 'edit', 'edit', NULL),
(38, 3, 'users', 'view', NULL, NULL),
(41, 3, 'users', 'edit', 'edit', 1);

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `hidden`, `developer`) VALUES
(1, 'Developer', 1, 1),
(2, 'Admin', 0, 0),
(3, 'User', 0, 0);

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `roles_id`, `first_name`, `last_name`, `email`, `password`, `salt`, `password_retrieval_key`, `phone`, `last_login`, `created_timestamp`) VALUES
(2, 1, 'Dev', 'User', 'dev_user', 'a3fd17fbea57884d1e8af38894aee084974bca2c', 'Jfh28dSoru', '', '', '2015-06-29 01:26:23', '2014-05-28 19:44:02');
